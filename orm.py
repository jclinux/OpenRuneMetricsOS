from lxml import html
import requests
import urllib.request
from pylab import *
import matplotlib as mpl
mpl.rcParams['font.size'] = 9.0
username = str(input("Your RS username: "))
page = requests.get("http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=" + username)
tree = html.fromstring(page.content)
statnums = tree.xpath('//td[@align="right"]/text()')
playerskills = []
statskills = ['Overall', 'Attack', 'Defence', 'Strength', 'Hitpoints', 'Ranged', 'Prayer', 'Magic', 'Cooking', 'Woodcutting', 'Fletching', 'Fishing', 'Firemaking', 'Crafting', 'Smithing', 'Mining', 'Herblore', 'Agility', 'Thieving', 'Slayer', 'Farming', 'Runecraft', 'Hunter', 'Construction']
for num in range(24):
    stats = tree.xpath('//a[@href="overall.ws?table=' + str(num) + '&user=' + username + '"' + ']/text()')
    for skill in statskills:
        if skill in str(stats):
            playerskills.append(skill)
finalstatslevel = []
count = 1
statnumcount = 0
for num in statnums:
    statnumcount += 1
for num in (range(int(statnumcount/3))):
    finalstatslevel.append(statnums[count])
    count +=3
finalstatsxp = []
count = 2
for num in (range(int(statnumcount/3))):
    finalstatsxp.append(statnums[count])
    count +=3
print("Skill - Level - XP")
for skill in (range(int(statnumcount/3))):
    print(playerskills[skill] + " - " + str(finalstatslevel[skill].replace(",", "")) + " - " + str(finalstatsxp[skill].replace(",", "")))
### Pie Graph Skill Levels / XP Note: Can be messy for certain players EX: PKers
figure(1, figsize=(6,6))
ax = axes([0.1, 0.1, 0.8, 0.8])
colors = ['red', 'green', 'blue', 'orange', 'gray', 'yellowgreen', 'white', 'purple', 'cyan', 'magenta']
fracs = finalstatslevel[1:]
pie(fracs, labels=playerskills[1:], colors=colors,
                autopct='%1.1f%%', shadow=False, startangle=90)
title(username.replace("+", " ") + "'s Stats (Skill Level)", bbox={'facecolor':'0.8', 'pad':5})
plt.savefig('skilllevel.png')
#######
