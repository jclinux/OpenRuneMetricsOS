OpenRuneMetricsOS (Old School)

This program an implementation of Jagex's RuneMetrics, but for Old School
RuneScape. The program also uses freely available data on Jagex's website. 
Meaning that if they were to change the website, then the program may need to
be updated or no longer work.

Reason:

The current way to view stats is on Jagex's website, however they do not offer
the RuneMetrics service to players. Things like XP/hour and other measurements
must be done by the player themselves or using 3rd party RuneScape clients which
are officially not allowed by Jagex. This program was made to ease the process
of tracking your stats down to a single command.

<hr>

OpenRuneMetricsOS v1.0.0
<p>
Implemented:

- Retrieve & display user stats

WIP:

- Display the user's stats in various graphs

To Do:

- Settings for different graphs and stats
    
- Record / compare stats
    
- Easier viewing of stats (Either PDF or HTML)
    
- Automatic tracking (Run in background)

<hr>

Installation/Setup:

This program requires 3 libraries that are available with pip.

    pip install lxml
    
    pip install requests
    
    pip install matplotlib

Once you have the libraries, just run...

    python orm.py

<hr>

Usage notes:
- In order for your stats to update, you must log out of the game.